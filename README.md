![alt text](https://gitlab.com/vision-pipeline-for-ros/vision-pipeline-for-ros/-/raw/master/icons/logo_long.svg)



# Vision Pipeline

[VPFR](https://gitlab.com/vision-pipeline-for-ros/vision-pipeline-for-ros)

[Wiki](https://gitlab.com/vision-pipeline-for-ros/vision-pipeline-for-ros/wikis/home)

---

# Multiple ArUco Marker Detection

This is an implementation of a ArUco Marker detection with OpenCV.  
There are several setting options.  

Details on this in the [wiki](https://gitlab.com/vision-pipeline-for-ros/algorithms/multiple_marker_aruco/-/wikis/home)
