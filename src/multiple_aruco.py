"""
multiple-marker-detection-vpfr
Copyright 2020 Vision Pipeline for ROS
Author: Sebastian Bergner <sebastianbergner5892@gmail.com>
SPDX-License-Identifier: MIT
"""
# todo -> add pushing data to rviz in tf2 format
# lookup the internship script & visualize data in new window

import cv2
import cv2.aruco as cvaruco
from vpfrmultiplemarkerdetection.msg import SingleMarker
from vpfrmultiplemarkerdetection.msg import MultipleMarker
from geometry_msgs.msg import Pose, Point, Quaternion
import rospy
from cv_bridge import CvBridge, CvBridgeError
from algorithm_template import AlgorithmTemplate
from sensor_msgs.msg import CameraInfo, Image
import numpy as np
import tf
import tf2_ros
from std_msgs.msg import Float64


class MultipleAruco(AlgorithmTemplate):
    
    def get_io_type(self):
        return Image, MultipleMarker

    def on_enable(self):
        rospy.loginfo("[algorithm][multiple_aruco] init")
        rospy.loginfo(
            "[algorithm][opencv_aruco] read camera info from "
            + self.__camera_settings_info_topic
        )
        self.__image_publish_topic = "/test/Image"
        self.__cam_info_sub = rospy.Subscriber(
            self.__camera_settings_info_topic, CameraInfo, self.cb_get_camera_info
        )
        self.__parameters = cvaruco.DetectorParameters_create()
        self.__bridge = CvBridge()
        self.__ros_to_cv2_encoding = cv2.COLOR_BGR2RGB
        self.__marker_length = 0.1
        self.__aruco_dict = cvaruco.Dictionary_get(cvaruco.DICT_4X4_250)
        self.__show_image = True
        self.__camera_matrix = None
        self.__dist_coeffs = None
        self.__publish_image = True
        rospy.loginfo("[algorithm][opencv_aruco] end")

    def on_disable(self):
        pass

    def main(self, req):
        cv_image = self.__bridge.imgmsg_to_cv2(req)
        cv_image = cv2.cvtColor(cv_image, self.__ros_to_cv2_encoding)
        corner_array, ids, rejectedImgPoints = cvaruco.detectMarkers(
            cv_image, self.__aruco_dict, parameters=self.__parameters
        )
        image = cv_image
        if len(corner_array) > 0:
            corner_counter = 0
            marker_array = []
            for corner in corner_array:
                marker_length = self.__marker_length
                camera_matrix = self.__camera_matrix
                dist_coeffs = self.__dist_coeffs
                rvec, tvec, _objPoints = cvaruco.estimatePoseSingleMarkers(
                    corner_array[0],
                    marker_length,
                    camera_matrix,
                    dist_coeffs
                )
                quat = tf.transformations.quaternion_from_euler(
                    float(rvec.flat[0]),
                    float(rvec.flat[1]),
                    float(rvec.flat[2]),
                )
                quaternion = Quaternion(quat[0], quat[1], quat[2], quat[3])
                point = Point(tvec.flat[0], tvec.flat[1], tvec.flat[2])
                pose = Pose(point, quaternion)
                # image = cvaruco.drawAxis(image, self.__camera_matrix, self.__dist_coeffs, rvec, tvec, self.__marker_length)
                marker_array.append(
                    SingleMarker(ids.flat[corner_counter], pose)
                )
                corner_counter += 1

            if self.__show_image:
                image = cvaruco.drawDetectedMarkers(
                    cv_image, corner_array, ids
                )
                self.show(image)
            return MultipleMarker(marker_array)
        if self.__show_image:
            self.show(image)
        return MultipleMarker(None)

    def cb_get_camera_info(self, info):
        if len(info.K) is 0 or len(info.D) is 0:
            rospy.logwarn("[algorithm][opencv_aruco] no camera info")
            self.__camera_matrix = np.array([[962.50986063965422, 0, 633.76485956492468], [0, 962.50986063965422, 381.90027365168055], [0, 0, 1]])
            self.__dist_coeffs = np.array([0, 0.024408088187719230, 0, 0, 0.043189876038211873])
        else:
            self.__camera_matrix = info.K
            self.__dist_coeffs = info.D
            self.__camera_matrix = np.reshape(self.__camera_matrix, (3, 3))
            self.__dist_coeffs = np.reshape(self.__dist_coeffs, (1, 5))
            self.__cam_info_sub.unregister()
            #self.camera_matrix = np.array([[962.50986063965422, 0, 633.76485956492468], [0, 962.50986063965422, 381.90027365168055], [0, 0, 1]])
            #self.dist_coeffs = np.array([0, 0.024408088187719230, 0, 0, 0.043189876038211873])


    # shows the processed image in window
    def show(self, image):
        cv2.imshow("frame", image)
        cv2.waitKey(1)

    def on_additional_data_change(self, data):
        if len(data) >= 1:
            self.__camera_settings_info_topic = data[0]
            rospy.loginfo(
                "[algorithm][opencv_aruco] read camera info from "
                + self.__camera_settings_info_topic
            )
            self.__cam_info_sub = rospy.Subscriber(
                self.__camera_settings_info_topic,
                CameraInfo,
                self.cb_get_camera_info,
            )
        else:
            rospy.loginfo("[algorithm][opencv_aruco] no additional data")

    def on_config_change(self, item):
        if item is None:
            return
        rostocv2encoding = item.get("rostocv2encoding")
        if rostocv2encoding is not None:
            if rostocv2encoding is not None or rostocv2encoding >= 0:
                rospy.loginfo(
                    "[algorithm][aruco] rostocv2encoding set to '%s'",
                    rostocv2encoding,
                )
                self.__ros_to_cv2_encoding = rostocv2encoding
            else:
                rospy.logwarn(
                    "[algorithm][aruco] rostocv2encoding type '%s' not found",
                    rostocv2encoding,
                )
        dictionaryname = item.get("dictionarytype")
        if dictionaryname is not None and isinstance(dictionaryname, int):
            dictionarytype = cvaruco.Dictionary_get(dictionaryname)
            if dictionarytype is not None:
                rospy.loginfo(
                    "[algorithm][aruco] dictionarytype set to '%s'",
                    dictionaryname,
                )
                self.__aruco_dict = dictionarytype
            else:
                rospy.logwarn(
                    "[algorithm][aruco] dictionarytype '%s' not found",
                    dictionaryname,
                )
        markerLength = item.get("markerLength")
        if markerLength is not None:
            if isinstance(markerLength, float):
                rospy.loginfo(
                    "[algorithm][aruco] markerLength set to '%s'", markerLength
                )
                self.__marker_length = float(markerLength)
            else:
                rospy.logwarn("[algorithm][aruco] markerLength is not a float")
